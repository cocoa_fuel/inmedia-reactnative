
import React from 'react'

import { StyleSheet, View, Image, TouchableOpacity, Text } from 'react-native';
import { color, font, appStrings } from '../components/Constant';

import Icon from 'react-native-vector-icons/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

class TabBarComponent extends React.Component {
  
  constructor(props){
    super(props);
    this.state = {
       
    }
  }

  

  render(){
    
    const { container, innerContainer, containerIconStyle, iconStyle } = styles;

    return (
      <View style={container}>
            <View style={innerContainer}>

              {this.props.navigation.state.index == 0 ?
                <TouchableOpacity style={{ flex: 1, justifyContent:'center', alignItems:'center' }}
                  hitSlop={{ left:20, right:20, top:20, bottom:20 }}
                  onPress={()=> this.props.navigation.navigate('Group') }>
                  <MaterialIcons name="home" size={30} color={color.white} />

                </TouchableOpacity>
                :
                <TouchableOpacity style={{ flex: 1, justifyContent:'center', alignItems:'center' }}
                  hitSlop={{ left:20, right:20, top:20, bottom:20 }}
                  onPress={()=> this.props.navigation.navigate('Group') }>
                  <MaterialIcons name="home" size={30} color={color.grey} />

                </TouchableOpacity>
              }

              {this.props.navigation.state.index == 1 ?
                <TouchableOpacity style={{ flex: 1, justifyContent:'center', alignItems:'center' }}
                  hitSlop={{ left:20, right:20, top:20, bottom:20 }}
                  onPress={()=> this.props.navigation.navigate('Notification') }>
                  <MaterialIcons name="watch-later" size={30} color={color.white} />

                </TouchableOpacity>
                :
                <TouchableOpacity style={{ flex: 1, justifyContent:'center', alignItems:'center', }}
                  hitSlop={{ left:20, right:20, top:20, bottom:20 }}
                  onPress={()=> this.props.navigation.navigate('Notification') }>
                  <MaterialIcons name="watch-later" size={30} color={color.grey} />
                </TouchableOpacity>
              }

              {this.props.navigation.state.index == 2 ?
                <TouchableOpacity style={{ flex: 1, justifyContent:'center', alignItems:'center' }}
                  hitSlop={{ left:20, right:20, top:20, bottom:20 }}
                  onPress={()=> this.props.navigation.navigate('Home') }>
                  <MaterialIcons name="whatshot" size={30} color={color.white} />
                </TouchableOpacity>
                :
                <TouchableOpacity style={{ flex: 1, justifyContent:'center', alignItems:'center' }}
                  hitSlop={{ left:20, right:20, top:20, bottom:20 }}
                  onPress={()=> this.props.navigation.navigate('Home') }>
                  <MaterialIcons name="whatshot" size={30} color={color.grey} />
                </TouchableOpacity>
              }

              
              


            </View>
		  </View>
    )
  }
}

export default TabBarComponent;

const styles = StyleSheet.create({
    container: {
      height: 50,
      backgroundColor: color.primary,
      margin: 0,
      padding:0,
      borderTopColor:'rgba(119, 131, 147, 0.5)',
     borderTopWidth: 0.5,
      // borderTopColor: color.silver
    },
    innerContainer: {
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'space-around',
      alignItems: 'center',
      margin: 0,
      padding:0,
      backgroundColor: color.primary,
    },
      containerIconStyle: {
      height: 60
    },
    iconStyle: {
      fontSize: 20,
      color: '#FFF'
    },
});
  