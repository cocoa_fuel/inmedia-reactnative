import React, { Component } from 'react';
import {
    Text, View,WebView,Platform,StyleSheet,StatusBar,ScrollView
} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Spinner from 'react-native-spinkit';
import Ionicons from 'react-native-vector-icons/Ionicons';

let WebViewRef;

   
    //Import your actions

class WebScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
           showError:false,
           isVisible:true,showWeb:true
        };
    }

    componentDidMount() {
    }
    reloadPage(){
        //this.setState({showError:false});
        WebViewRef.reload() 
    }
    renderLoader(){
        return (<Spinner style={{top:100,alignSelf:'center'}} isVisible={true} size={50} type={'Pulse'} color={'#04b2e2'}/>);

    }
    renderError(){
        return (<View style={{alignSelf:'center',top:100}} >
        <MaterialIcons name="error-outline" size={50} style={{alignSelf:'center'}} color="#e74c3c" />
        <Text style={{color:'#34495e',fontSize:30}}>Sorry</Text>

        <Text style={{color:'#7f8c8d'}}>Could not load the page you are looking for.</Text>
        <MaterialIcons name="refresh" size={50} style={{alignSelf:'center',top:17}}  onPress={()=>WebViewRef.reload() } color="#1abc9c" />

        </View>);
    }
  render() {
    const { navigation } = this.props;
    const title = navigation.getParam('name', 'Details');
    const pageLink = navigation.getParam('link', 'www.google.com');


    return (


        <View style={{flex:1}}>
            <View style={styles.header}>
                    <View style={styles.headerInner}>


                        <Text style={styles.headerText}>{title}</Text>
                        
                        <Ionicons name="md-close" size={26} onPress={()=>this.props.navigation.pop()} color={'white'} />


                    </View>
                </View>
        
                <WebView
         ref={WEBVIEW_REF => (WebViewRef = WEBVIEW_REF)}
        source={{uri: pageLink}}
        //onLoadStart={()=>this.setState({showWeb:false})}
        //onLoadEnd={()=>this.setState({isVisible:false,showWeb:true})}
        startInLoadingState={true}
        javaScriptEnabled={true}
         domStorageEnabled={true}
         renderError={this.renderError}

        renderLoading={this.renderLoader}
        style={{justifyContent: 'center',
        alignItems: 'center',
        flex:1,
        }}
        //style={{marginTop: (Platform.OS === 'ios') ? 20 : 0}}
        onError={()=>this.setState({showError:true,isVisible:false})}
      />
              </View>

   
        );
  }
}


  
export default WebScreen;
const styles = StyleSheet.create({

    headerInner: {
        flex: 1,
        backgroundColor: 'transparent',
        flexDirection: 'row'
    },
    headerText: {
        flexDirection: 'row',
        flex: 1,
        textAlign: 'center',
        backgroundColor: 'transparent',
        fontSize: 19,
        color: 'white',
        fontWeight: '400'
    },
    header: {
        height: 50,
        paddingHorizontal: 13,
        alignItems: 'center',
        //justifyContent: 'center',
        marginTop: Platform.OS == "ios" ? 20 : 0,
        flexDirection: 'row',

        backgroundColor: '#2980b9'
    },

});
