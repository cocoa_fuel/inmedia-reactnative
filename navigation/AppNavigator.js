
import { Text, Animated, Easing, } from 'react-native'
import { createStackNavigator, createSwitchNavigator } from 'react-navigation'
import WebScreen from './WebScreen'
import SearchScreen from './SearchScreen'

import TabNavigator from './BottomTabs';

// https://github.com/react-community/react-navigation/issues/1254
const noTransitionConfig = () => ({
  transitionSpec: {
    duration: 0,
    timing: Animated.timing,
    easing: Easing.step0
  }
})

const DashBoard = createStackNavigator({
  tabNavigator: { screen: TabNavigator, navigationOptions: () => ({
    drawerLockMode: 'locked-close'
  })
},
  webScreen : {screen :WebScreen},
  search: {screen : SearchScreen}
}, {
   headerMode: 'none',
   initialRouteName: 'tabNavigator',
})







export default SwitchNavigator  = createStackNavigator(
  {
    
    App: DashBoard,
  },
  {
    headerMode:'none',
    transitionConfig: noTransitionConfig,
  }
);