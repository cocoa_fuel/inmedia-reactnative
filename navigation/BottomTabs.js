import React from 'react';
import { createBottomTabNavigator, createStackNavigator, createDrawerNavigator,DrawerNavigator } from 'react-navigation';
import GroupScreen from '../Screens/group';
import NotificationScreen from '../Screens/notification';
import HomeScreen from '../Screens/home';
import SearchScreen from '../Screens/search';
import ProfileScreen from '../Screens/profile';
import DrawerContainer from './DrawerContainer';
import TabBarComponent from './TabBarComponent';

const GroupStack = createStackNavigator({
  groupScreen: { screen: GroupScreen, navigationOptions: ({ navigation }) => ({
    drawerLockMode: 'locked-close'
  })  },
}, {
   headerMode: 'none',
   initialRouteName: 'groupScreen',
})

GroupStack.navigationOptions={ 
  drawerLockMode: 'locked-close'
}

const NotificationStack = createStackNavigator({
  notificationScreen: { screen: NotificationScreen,navigationOptions: () => ({
    drawerLockMode: 'locked-close'
  }) },
}, {
   headerMode: 'none',
   initialRouteName: 'notificationScreen',
})

const HomeStack = createStackNavigator({
  homeScreen: { screen: HomeScreen },
}, {
   headerMode: 'none',
   initialRouteName: 'homeScreen',
})

const ProfileStack = createStackNavigator({
  profileScreen: { screen: ProfileScreen  },
}, {
   headerMode: 'none',
   initialRouteName: 'profileScreen',
})


const SearchStack = createStackNavigator({
  searchScreen: { screen: SearchScreen },
}, {
   headerMode: 'none',
   initialRouteName: 'searchScreen',
})


const bottomTabs = createBottomTabNavigator(
  {
    Group: { screen: GroupStack,navigationOptions: ({ navigation }) => ({
      drawerLockMode:'locked',  //here
    }) },
    Notification: { screen: NotificationStack },
    Home: { screen: HomeStack },
    Search: { screen: SearchStack },
    Profile: { screen: ProfileStack},
  },
  {
    // navigationOptions: ({ navigation }) => ({
    //   tabBarIcon: ({ focused, tintColor }) => {
    //     const { routeName } = navigation.state;
    //     let iconName;
    //     if (routeName === 'Home') {
    //       iconName = `ios-home`;
    //       return <Ionicons name={iconName} size={30} color={tintColor} />
    //     } else if (routeName === 'Notification') {
    //       iconName = `ios-notifications`;
    //       return <Ionicons name={iconName} size={30} color={tintColor} />
    //     } else if (routeName === 'Search') {
    //         iconName = `ios-search`;
    //         return <Ionicons name={iconName} size={30} color={tintColor} />
    //     } else if (routeName === 'Profile') {
    //         iconName = `user-circle`;
    //         return <FontAwesome name={iconName} size={30} color={tintColor} />
    //     } else if (routeName === 'Group') {
    //         iconName = `users`;
    //         return <FontAwesome name={iconName} size={25} color={tintColor} />
    //     }

    //     // You can return any component that you like here! We usually use an
    //     // icon component from react-native-vector-icons
    //     //return <Ionicons name={iconName} size={25} color={tintColor} />;
    //   },
    // }),
    // tabBarComponent: TabBarBottom,
    // tabBarPosition: 'bottom',
    tabBarComponent: props => <TabBarComponent {...props} />,
    animationEnabled: false,
    
    swipeEnabled: false,
    tabBarOptions: {
      showLabel: false,
      activeTintColor: '#1CCBFB',
		  inactiveTintColor: '#818C9D',
      style: {
            backgroundColor: '#fff',
            borderTopWidth: 0,
		  },
    },
    initialRouteName: 'Group',
  }
);

export default DrawerNavigator({
  bottomTabs: { screen: bottomTabs},
}, {
  
  contentComponent: DrawerContainer,
});