import React from 'react'
import { StyleSheet, Text, View, Image, TextInput, TouchableOpacity,
  ScrollView, } from 'react-native'
  import Ionicons from 'react-native-vector-icons/Ionicons';

export default class DrawerContainer extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
    }
  }

  render() {
    const { navigation } = this.props;
    
    return (
      
      <View style={{flex:1,backgroundColor:'#f1f2f6'}}>

        
        <View style={{alignSelf:'center',paddingVertical:10,alignItems:'flex-start',}}>
          <Text style={{fontSize:20,fontWeight:"500",color:"#2f3542"}}>Menu</Text>
        </View>
        <ScrollView style={{flexDirection:'column', paddingTop:0, }}>
          <View style={{paddingHorizontal:6}}>
          <TouchableOpacity onPress={()=>this.props.navigation.navigate('webScreen',{link:'https://www.inmedia.ba/category/inmediadanas/',name:'INMEDIA DANAS'})} style={{flex:1,height:60,width:'100%',flexDirection:"row",paddingVertical:7,alignItems:'center'}}>
          <Text style={{fontSize:15,color:'#2f3542'}}>INMEDIA DANAS</Text>
          <View style={{flex:1,flexDirection:'row',justifyContent:'flex-end',paddingHorizontal:9}}>
          <Ionicons style={{}} name="ios-arrow-forward" color="#04b2e2" size={24} />  
          </View>

          </TouchableOpacity>
          <View style={styles.break}/>
          <TouchableOpacity onPress={()=>this.props.navigation.navigate('webScreen',{link:'https://www.inmedia.ba/category/lokalne-teme/',name:'LOKALNE TEME'})} style={{flex:1,height:60,width:'100%',flexDirection:"row",paddingVertical:7,alignItems:'center'}}>
          <Text style={{fontSize:15,color:'#2f3542'}}>LOKALNE TEME</Text>
          <View style={{flex:1,flexDirection:'row',justifyContent:'flex-end',paddingHorizontal:9}}>

          <Ionicons style={{alignSelf:'flex-end'}} name="ios-arrow-forward" color="#04b2e2" size={24} />  
          </View>

          </TouchableOpacity>
          <View style={styles.break}/>

          <TouchableOpacity onPress={()=>this.props.navigation.navigate('webScreen',{link:'https://www.inmedia.ba/category/vijesti/',name:'VIJESTI'})} style={{flex:1,height:60,width:'100%',flexDirection:"row",paddingVertical:7,alignItems:'center'}}>
          <Text style={{fontSize:15,color:'#2f3542'}}>VIJESTI</Text>
          <View style={{flex:1,flexDirection:'row',justifyContent:'flex-end',paddingHorizontal:9}}>

          <Ionicons style={{alignSelf:'flex-end'}} name="ios-arrow-forward" color="#04b2e2" size={24} />  
          </View>
          </TouchableOpacity>

          <View style={styles.break}/>

          <TouchableOpacity onPress={()=>this.props.navigation.navigate('webScreen',{link:'https://www.inmedia.ba/category/sport-2/',name:'SPORT'})} style={{flex:1,height:60,width:'100%',flexDirection:"row",paddingVertical:7,alignItems:'center'}}>
          <Text style={{fontSize:15,color:'#2f3542'}}>SPORT</Text>
          <View style={{flex:1,flexDirection:'row',justifyContent:'flex-end',paddingHorizontal:9}}>

          <Ionicons style={{alignSelf:'flex-end'}} name="ios-arrow-forward" color="#04b2e2" size={24} />  
          </View>
          </TouchableOpacity>

          <View style={styles.break}/>


          <TouchableOpacity onPress={()=>this.props.navigation.navigate('webScreen',{link:'https://www.inmedia.ba/category/blog/',name:'BLOG'})} style={{flex:1,height:60,width:'100%',flexDirection:"row",paddingVertical:7,alignItems:'center'}}>
          <Text style={{fontSize:15,color:'#2f3542'}}>BLOG</Text>
          <View style={{flex:1,flexDirection:'row',justifyContent:'flex-end',paddingHorizontal:9}}>

          <Ionicons style={{alignSelf:'flex-end'}} name="ios-arrow-forward" color="#04b2e2" size={24} />  
          </View>
          </TouchableOpacity>

          <View style={styles.break}/>

          <TouchableOpacity onPress={()=>this.props.navigation.navigate('webScreen',{link:'https://www.inmedia.ba/category/nase-price/',name:'#VISITSANA'})} style={{flex:1,height:60,width:'100%',flexDirection:"row",paddingVertical:7,alignItems:'center'}}>
          <Text style={{fontSize:15,color:'#2f3542'}}>#VISITSANA</Text>
          <View style={{flex:1,flexDirection:'row',justifyContent:'flex-end',paddingHorizontal:9}}>

          <Ionicons style={{alignSelf:'flex-end'}} name="ios-arrow-forward" color="#04b2e2" size={24} />  
          </View>
          </TouchableOpacity>
          </View>

          <View style={{height:7,width:'100%',backgroundColor:'#dfe4ea'}}>


          </View>
          <View style={{paddingHorizontal:6}}>

          <TouchableOpacity onPress={()=>this.props.navigation.navigate('webScreen',{link:'https://www.inmedia.ba/kontakt/',name:'KONTAKT'})} style={{flex:1,height:60,width:'100%',flexDirection:"row",paddingVertical:7,alignItems:'center'}}>
          <Text style={{fontSize:15,color:'#2980b9'}}>KONTAKT</Text>
          <View style={{flex:1,flexDirection:'row',justifyContent:'flex-end',paddingHorizontal:9}}>

          </View>
          </TouchableOpacity>
          <View style={styles.break}/>
          <TouchableOpacity onPress={()=>this.props.navigation.navigate('webScreen',{link:'https://www.inmedia.ba/o-nama/',name:'O NAMA'})} style={{flex:1,height:60,width:'100%',flexDirection:"row",paddingVertical:7,alignItems:'center'}}>
          <Text style={{fontSize:15,color:'#2980b9'}}>O NAMA</Text>
          <View style={{flex:1,flexDirection:'row',justifyContent:'flex-end',paddingHorizontal:9}}>

          </View>
          </TouchableOpacity>
          <View style={styles.break}/>
          <TouchableOpacity onPress={()=>this.props.navigation.navigate('webScreen',{link:'https://www.inmedia.ba/uslovi-koristenja/',name:'KORIŠTENJA'})} style={{flex:1,height:60,width:'100%',flexDirection:"row",paddingVertical:7,alignItems:'center'}}>
          <Text style={{fontSize:15,color:'#2980b9'}}>USLOVI KORIŠTENJA</Text>
          <View style={{flex:1,flexDirection:'row',justifyContent:'flex-end',paddingHorizontal:9}}>

          </View>
          </TouchableOpacity>
          </View>



          

          
              
          
        </ScrollView>

      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  break: {
    width:'100%',
    borderColor: '#dadfe1',
    borderBottomWidth: 0.5,
},

rowContainer: {
  flexDirection:'row', justifyContent:'flex-start', height:40, alignItems:'center'
},
rowText : {
  alignItems:'center', marginHorizontal:10
},

});

