import React, { Component } from 'react';
import {
    Text, View,WebView,Platform,StyleSheet,StatusBar,ScrollView,Image,TextInput
} from 'react-native';
//import {color, font, apiURLs} from '../../components/Constant';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';

import Spinner from 'react-native-spinkit';
let WebViewRef;

   
    //Import your actions

class SearchScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
           showError:false,
           query:'',
           isVisible:true,showWeb:true,webUri:'https://www.inmedia.ba/?s='
        };
    }

    componentDidMount() {
    }
    reloadPage(){
        //this.setState({showError:false});
        WebViewRef.reload() 
    }
    renderLoader(){
        return (<Spinner style={{top:100,alignSelf:'center'}} isVisible={true} size={50} type={'Pulse'} color={'#04b2e2'}/>);

    }
    renderError(){
        return (<View style={{alignSelf:'center',top:100}} >
        <MaterialIcons name="error-outline" size={50} style={{alignSelf:'center'}} color="#e74c3c" />
        <Text style={{color:'#34495e',fontSize:30}}>Sorry</Text>

        <Text style={{color:'#7f8c8d'}}>Could not load the page you are looking for.</Text>
        <MaterialIcons name="refresh" size={50} style={{alignSelf:'center',top:17}}  onPress={()=>WebViewRef.reload() } color="#1abc9c" />

        </View>);
    }
    searchWeb(){
        var que = this.state.query
        this.setState({webUri:'https://www.inmedia.ba/?s='+que})
        this.reloadPage();
    }
  render() {

    return (

<View style={{flex:1}}>
<View style={styles.header}>
<Ionicons name="md-arrow-back" size={24} style={{padding:6}} onPress={()=>this.props.navigation.pop()} color={'black'} />

        <View style={styles.headerInner}>

        <TextInput 
        ref={ref => this.textInputRef = ref}        
        placeholder="Search.."
      selectionColor="#636e72"
      returnKeyType="search"
      textBreakStrategy="highQuality"
       underlineColorAndroid='transparent'
       autoCorrect={false}
       blurOnSubmit={true}
       autoFocus={true}
       onSubmitEditing={()=>this.searchWeb()}
       onChangeText={(text)=>this.setState({query:text})}
       //onSubmitEditing={()=>this.onSubmit()}
       autoCapitalize='none'
       placeholderTextColor="#636e72"
        style={{height:"100%",width:'70%',fontSize:17,paddingLeft:15,color:'#718093'}}
        
        
        
        />
        </View>
{/*         <Ionicons name="md-search" size={28} style={{padding:6}} onPress={()=>this.props.navigation.openDrawer()} color={'black'} />
 */}
      </View>
 <WebView
         ref={WEBVIEW_REF => (WebViewRef = WEBVIEW_REF)}
        source={{uri: this.state.webUri}}
        //onLoadStart={()=>this.setState({showWeb:false})}
        //onLoadEnd={()=>this.setState({isVisible:false,showWeb:true})}
        startInLoadingState={true}
        javaScriptEnabled={true}
         domStorageEnabled={true}
         renderError={this.renderError}

        renderLoading={this.renderLoader}
        style={{justifyContent: 'center',
        alignItems: 'center',
        flex:1,
        }}
        //style={{marginTop: (Platform.OS === 'ios') ? 20 : 0}}
        onError={()=>this.setState({showError:true,isVisible:false})}
      />
</View>

        
               
   
        );
  }
}


  
export default SearchScreen;

const styles = StyleSheet.create({
    header:{
        height:52   ,
        paddingHorizontal:13,
         alignItems:'center',
         marginTop: Platform.OS == "ios" ? 20 : 0,
        flexDirection:'row',
        borderBottomColor:'rgba(99, 110, 114, 0.2)',
        borderBottomWidth:1,
        backgroundColor:'white'},
        headerInner:{
            flex: 1,
            justifyContent:'center',
            
            alignItems:'center',
                backgroundColor: 'transparent',
                flexDirection: 'row'
         },
})