import React from 'react';
import {SafeAreaView,StatusBar} from 'react-native';
import SwitchNavigator from './navigation/AppNavigator';
import SplashScreen from 'react-native-splash-screen'


export default class App extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      isReady: false,
    };
    
  }
  

  componentDidMount(){
    SplashScreen.hide();

  }

  async componentWillMount() {
    StatusBar.setHidden(true);
    this.setState({ isReady: true });
  }

  render() {
    return (
<SafeAreaView style={{flex: 1, backgroundColor: '#fff'}}>
            <SwitchNavigator />
            </SafeAreaView>
          
    );
  }
}
