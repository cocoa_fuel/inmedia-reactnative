const color = {
    primary: '#fff',
    black: '#000000',
    white: '#1CCBFB',
    lightGrey:'#D3D3D3',
    grey:'#818C9D',
    darkgrey: 'rgb(74,74,74)',
    red : 'rgb(208,2,27)',
    transparent: 'transparent'
};

const font = {
    sansLight: 'SourceSansPro-Light',
    sansRegular: 'SourceSansPro-Regular',
    sansSemibold: 'SourceSansPro-Semibold',
};

const appStrings = {
    appName: 'Defaullt',
};

const apiURLs = {

    baseURL: 'https://warm-cliffs-67509.herokuapp.com',
    // baseURL: 'http://192.168.100.27:4040'
}

export { color, font, appStrings, apiURLs };
