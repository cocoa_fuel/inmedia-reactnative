import React, { Component } from 'react';
import {
    Text, View,WebView,Platform,StyleSheet,StatusBar,ScrollView,Image
} from 'react-native';
import {color, font, apiURLs} from '../../components/Constant';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Spinner from 'react-native-spinkit';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {navigationOptions} from 'react-navigation'
let WebViewRef;

   
    //Import your actions

class NotificationScreen extends Component {
    static navigationOptions = {
        drawerLockMode: 'locked-closed'
    }
    constructor(props) {
        super(props);
        this.state = {
           showError:false,
           isVisible:true,showWeb:true
        };
    }

    componentDidMount() {
    }
    reloadPage(){
        //this.setState({showError:false});
        WebViewRef.reload() 
    }
    renderLoader(){
        return (<Spinner style={{top:100,alignSelf:'center'}} isVisible={true} size={50} type={'Pulse'} color={'#04b2e2'}/>);

    }
    renderError(){
        return (<View style={{alignSelf:'center',top:100}} >
        <MaterialIcons name="error-outline" size={50} style={{alignSelf:'center'}} color="#e74c3c" />
        <Text style={{color:'#34495e',fontSize:30}}>Sorry</Text>

        <Text style={{color:'#7f8c8d'}}>Could not load the page you are looking for.</Text>
        <MaterialIcons name="refresh" size={50} style={{alignSelf:'center',top:17}}  onPress={()=>WebViewRef.reload() } color="#1abc9c" />

        </View>);
    }
  render() {

    return (



<View style={{flex:1}}>
<View style={styles.header}>
<Ionicons name="ios-menu" size={28} style={{padding:6}} onPress={()=>this.props.navigation.openDrawer()} color={'black'} />

        <View style={styles.headerInner}>

<Image source={require('../../asset/images/header.png')} style={{width:90,height:52}} resizeMode="contain" />
        </View>
        <Ionicons name="md-search" size={28} style={{padding:6}} onPress={()=>this.props.navigation.navigate('search')} color={'black'} />

      </View>
        
                <WebView
         ref={WEBVIEW_REF => (WebViewRef = WEBVIEW_REF)}
        source={{uri: 'https://www.inmedia.ba/najnovije/'}}
        //onLoadStart={()=>this.setState({showWeb:false})}
        //onLoadEnd={()=>this.setState({isVisible:false,showWeb:true})}
        startInLoadingState={true}
        javaScriptEnabled={true}
         domStorageEnabled={true}
         renderError={this.renderError}

        renderLoading={this.renderLoader}
        style={{justifyContent: 'center',
        alignItems: 'center',
        flex:1,
        }}
        //style={{marginTop: (Platform.OS === 'ios') ? 20 : 0}}
        onError={()=>this.setState({showError:true,isVisible:false})}
      />
      </View>
   
        );
  }
}


  
export default NotificationScreen;
const styles = StyleSheet.create({
    header:{
        height:52   ,
        paddingHorizontal:13,
         alignItems:'center',
         marginTop: Platform.OS == "ios" ? 20 : 0,
        flexDirection:'row',
        borderBottomColor:'rgba(99, 110, 114, 0.2)',
        borderBottomWidth:1,
        backgroundColor:'white'},
        headerInner:{
            flex: 1,
            justifyContent:'center',
            
            alignItems:'center',
                backgroundColor: 'transparent',
                flexDirection: 'row'
         },
})