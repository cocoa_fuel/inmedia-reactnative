import React, { Component } from 'react';
import {
  View, ActivityIndicator, StatusBar, AsyncStorage,
} from 'react-native';
import {color, font, apiURLs} from '../components/Constant';
   
   


class MainApp extends Component {

  constructor(props) {
    super(props);
    this.state = {
        userDetail : this.props.userDetail,
      };
    this._bootstrapAsync();  
  }
  
  _bootstrapAsync = async () => {
    this.props.navigation.navigate('App');
    
  };

  render() {
    return (
      <View style={{ flex:1, justifyContent:'center', alignItems:'center' }}>
        <ActivityIndicator />
        <StatusBar barStyle="default" />
      </View>
    );
  }

}

export default MainApp;
